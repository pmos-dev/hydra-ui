#!/bin/bash
echo Removing existing deployment
rm -rf dist
rm -rf ../../node/hydranode/angular
#echo Running commons.sh
#./commons.sh
echo Building production deployment
ng build --prod --base-href /hydra/angular/ --build-optimizer=true && echo "Moving to node" && mv dist/hydra ../../node/hydranode/angular && echo "Deployed."
