import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsStockModule } from 'ngx-webappcommons-stock';

const appRoutes: Routes = [
		{
			path: 'hydra',
			loadChildren: () => import('./hydra/hydra.module').then(m => m.HydraModule)
		},
		{
			path: '',
			pathMatch: 'full',
			redirectTo: 'hydra'
		}
];

@NgModule({
	imports: [
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsStockModule,
		RouterModule.forRoot(
				appRoutes,
				{
						enableTracing: false,
						scrollPositionRestoration: 'enabled'
				}
		)
	],
	declarations: [
	],
	exports: [
		RouterModule
	],
	providers: [
	]
})
export class AppRoutingModule { }
