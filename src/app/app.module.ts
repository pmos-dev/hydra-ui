import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsStockModule } from 'ngx-webappcommons-stock';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
	overrides = {
		// override hammerjs default configuration
		swipe: {
			direction: 31 // /! ugly hack to allow swipe in all direction
		},
		pan: {
			direction: 30 // /! ugly hack to allow pan in all direction
		},
		press: {
			time: 600
		}
	} as any;
}


@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		FormsModule,
		NgxAngularCommonsCoreModule.forRoot(),
		NgxAngularCommonsAppModule.forRoot(),
		NgxWebAppCommonsCoreModule.forRoot(),
		NgxWebAppCommonsAppModule.forRoot(),
		NgxWebAppCommonsMobileModule.forRoot(),
		NgxWebAppCommonsStockModule.forRoot(),
		SharedModule.forRoot(),
		AppRoutingModule
	],
	declarations: [
		AppComponent
	],
	providers: [
		{
			provide: HAMMER_GESTURE_CONFIG,
			useClass: MyHammerConfig
		}
	],
	bootstrap: [ AppComponent ]
})
export class AppModule { }
