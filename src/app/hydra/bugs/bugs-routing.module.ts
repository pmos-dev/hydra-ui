import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BugsComponent } from './components/bugs/bugs.component';
import { PhpComponent } from './components/php/php.component';
import { AspComponent } from './components/asp/asp.component';
import { OverviewComponent } from './components/overview/overview.component';

import { PhpErrorResolver } from './resolvers/php-error.resolver';
import { AspErrorResolver } from './resolvers/asp-error.resolver';

const bugsRoutes: Routes = [
		{
			path: '',
			component: BugsComponent,
			pathMatch: 'prefix',
			children: [
					{
						path: 'overview',
						component: OverviewComponent
					},
					{
						path: 'php',
						component: PhpComponent,
						resolve: {
							errors: PhpErrorResolver
						}
					},
					{
						path: 'asp',
						component: AspComponent,
						resolve: {
							errors: AspErrorResolver
						}
					},
					{
						path: '',
						redirectTo: 'overview'
					}
			]
		}
];

@NgModule({
	imports: [
		RouterModule.forChild(bugsRoutes)
	],
	exports: [
		RouterModule
	]
})
export class BugsRoutingModule { }
