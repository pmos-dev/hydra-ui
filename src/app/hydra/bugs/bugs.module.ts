import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';

import { BugsComponent } from './components/bugs/bugs.component';
import { PhpComponent } from './components/php/php.component';
import { AspComponent } from './components/asp/asp.component';
import { OverviewComponent } from './components/overview/overview.component';

import { RestService } from './services/rest.service';

import { PhpErrorResolver } from './resolvers/php-error.resolver';
import { AspErrorResolver } from './resolvers/asp-error.resolver';

import { BugsRoutingModule } from './bugs-routing.module';

@NgModule({
	imports: [
			CommonModule,
			NgxWebAppCommonsCoreModule,
			NgxWebAppCommonsMobileModule,
			BugsRoutingModule
	],
	declarations: [
			BugsComponent,
			PhpComponent,
			AspComponent,
			OverviewComponent
	],
	providers: [
			RestService,
			PhpErrorResolver,
			AspErrorResolver
	]
})
export class BugsModule { }
