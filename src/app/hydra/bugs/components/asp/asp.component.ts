import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { IUrlErrors } from 'hydra-crawler-ts-assets';
import { TAspError } from 'hydra-crawler-ts-assets';

@Component({
		templateUrl: './asp.component.html',
		styleUrls: ['./asp.component.less']
})
export class AspComponent extends CommonsComponent {
	errors: IUrlErrors<TAspError>[] = [];
	
	constructor(
			private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		this.subscribe(
				this.activatedRoute.data,
				(data: {
						errors: IUrlErrors<TAspError>[];
				}): void => {
					this.errors = data.errors
							.filter((ue: IUrlErrors<TAspError>): boolean => {
								return ue.errors
										.filter((error: (TAspError|true)): boolean => error !== true)
										.length > 0;
							});
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
