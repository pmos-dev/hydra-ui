import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	templateUrl: './bugs.component.html',
	styleUrls: ['./bugs.component.less']
})
export class BugsComponent extends CommonsComponent {

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
