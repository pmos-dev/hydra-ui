import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { IUrlErrors } from 'hydra-crawler-ts-assets';
import { TPhpError } from 'hydra-crawler-ts-assets';
import { EPhpErrorType } from 'hydra-crawler-ts-assets';

@Component({
		templateUrl: './php.component.html',
		styleUrls: ['./php.component.less']
})
export class PhpComponent extends CommonsComponent {
	errors: IUrlErrors<TPhpError>[] = [];

	constructor(
			private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		this.subscribe(
				this.activatedRoute.data,
				(data: {
						errors: IUrlErrors<TPhpError>[];
				}): void => {
					this.errors = data.errors
							.filter((ue: IUrlErrors<TPhpError>): boolean => {
								return ue.errors
										.filter((error: TPhpError): boolean => error.type === EPhpErrorType.FATAL)
										.length > 0;
							});
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
