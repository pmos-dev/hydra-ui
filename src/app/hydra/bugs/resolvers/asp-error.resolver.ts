import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { IUrlErrors } from 'hydra-crawler-ts-assets';
import { TAspError } from 'hydra-crawler-ts-assets';

import { RestService } from '../services/rest.service';

@Injectable()
export class AspErrorResolver implements Resolve<IUrlErrors<TAspError>[]> {
	constructor(
			private restService: RestService
	) {}

	resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<IUrlErrors<TAspError>[]> {
		return this.restService.listAspErrors();
	}
}
