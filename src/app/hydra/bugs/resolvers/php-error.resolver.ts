import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { TPhpError } from 'hydra-crawler-ts-assets';
import { IUrlErrors } from 'hydra-crawler-ts-assets';

import { RestService } from '../services/rest.service';

@Injectable()
export class PhpErrorResolver implements Resolve<IUrlErrors<TPhpError>[]> {
	constructor(
			private restService: RestService
	) {}

	resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<IUrlErrors<TPhpError>[]> {
		return this.restService.listPhpErrors();
	}
}
