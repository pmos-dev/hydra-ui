import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { IUrlErrors, isIUrlErrors } from 'hydra-crawler-ts-assets';
import { TPhpError, isTPhpError } from 'hydra-crawler-ts-assets';
import { TAspError, isTAspError } from 'hydra-crawler-ts-assets';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';

@Injectable()
export class RestService extends CommonsRestService {
	constructor(
			configService: CommonsConfigService,
			http: HttpClient
	) {
		super(
				CommonsType.assertString(configService.getString('rest', 'url')),
				http
		);
	}
	
	//----------------------------------------

	public async listPhpErrors(): Promise<IUrlErrors<TPhpError>[]> {
		const rest: unknown = await super.get<IUrlErrors<TPhpError>[]>('bugs/php');
		if (!CommonsType.isTArray<IUrlErrors<TPhpError>>(
				rest,
				(t: unknown): t is IUrlErrors<TPhpError> => isIUrlErrors<TPhpError>(t, isTPhpError)
		)) throw new Error('Did not return an array of IUrlErrors');

		return rest;
	}

	public async listAspErrors(): Promise<IUrlErrors<TAspError>[]> {
		const rest: unknown = await super.get<IUrlErrors<TPhpError>[]>('bugs/asp');
		if (!CommonsType.isTArray<IUrlErrors<TAspError>>(
				rest,
				(t: unknown): t is IUrlErrors<TAspError> => isIUrlErrors<TAspError>(t, isTAspError)
		)) throw new Error('Did not return an array of IUrlErrors');

		return rest;
	}
}
