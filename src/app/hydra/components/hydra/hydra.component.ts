import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'hydra',
	templateUrl: './hydra.component.html',
	styleUrls: ['./hydra.component.less']
})
export class HydraComponent extends CommonsComponent {
	title: string = 'Hydra';
	subtitle: string = '';

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	doSettings(): void {
		this.router.navigate([ '/', 'hydra', 'settings' ], { relativeTo: this.activatedRoute });
	}
}
