import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'crawl',
	templateUrl: './crawl.component.html',
	styleUrls: ['./crawl.component.less']
})
export class CrawlComponent extends CommonsComponent {
	constructor(
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
