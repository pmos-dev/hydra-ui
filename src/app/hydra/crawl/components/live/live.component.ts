import { Component } from '@angular/core';

import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'live',
	templateUrl: './live.component.html',
	styleUrls: ['./live.component.less']
})
export class LiveComponent extends CommonsComponent {
	EStatus = EStatus;
	
	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
