import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TDomainQueue } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'queue',
	templateUrl: './queue.component.html',
	styleUrls: ['./queue.component.less']
})
export class QueueComponent extends CommonsComponent {
	queue: TDomainQueue[] = [];

	constructor(
			private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		this.activatedRoute.data.subscribe((data: any): void => {
			this.queue = data['queue']
					.filter((queued: TDomainQueue): boolean => queued.queue > 100)
					.sort((a: TDomainQueue, b: TDomainQueue): number => {
						if (a.queue < b.queue) return -1;
						if (a.queue > b.queue) return 1;
						return 0;
					})
					.reverse();
		});
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
