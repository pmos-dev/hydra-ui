import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CrawlComponent } from './components/crawl/crawl.component';
import { LiveComponent } from './components/live/live.component';
import { QueueComponent } from './components/queue/queue.component';

import { QueuedsResolver } from './resolvers/queueds.resolver';

const crawlRoutes: Routes = [
		{
			path: '',
			component: CrawlComponent,
			pathMatch: 'prefix',
			children: [
					{
						path: 'live',
						component: LiveComponent,
						resolve: {
						}
					},
					{
						path: 'queue',
						component: QueueComponent,
						resolve: {
								queue: QueuedsResolver
						}
					},
					{
						path: '',
						redirectTo: 'live'
					}
			]
		}
];

@NgModule({
	imports: [
		RouterModule.forChild(crawlRoutes)
	],
	exports: [
		RouterModule
	]
})
export class CrawlRoutingModule { }
