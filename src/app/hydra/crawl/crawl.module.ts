import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';

import { NgxGraphicsCommonsWidgetModule } from 'ngx-graphicscommons-widget';

import { SharedModule } from 'shared/shared.module';

import { CrawlComponent } from './components/crawl/crawl.component';
import { LiveComponent } from './components/live/live.component';
import { QueueComponent } from './components/queue/queue.component';

import { RestService } from './services/rest.service';

import { QueuedsResolver } from './resolvers/queueds.resolver';

import { CrawlRoutingModule } from './crawl-routing.module';

@NgModule({
	imports: [
		CommonModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxWebAppCommonsMobileModule,
		NgxGraphicsCommonsWidgetModule,
		SharedModule,
		CrawlRoutingModule
	],
	providers: [
		RestService,
		QueuedsResolver
	],
	declarations: [
		CrawlComponent,
		LiveComponent,
		QueueComponent
	]
})
export class CrawlModule { }
