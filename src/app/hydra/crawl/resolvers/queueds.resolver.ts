import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { TDomainQueue } from 'hydra-crawler-ts-assets';

import { RestService } from '../services/rest.service';

@Injectable()
export class QueuedsResolver implements Resolve<TDomainQueue[]> {
	constructor(
			private restService: RestService
	) {}

	resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<TDomainQueue[]> {
		return this.restService.listQueueds();
	}
}
