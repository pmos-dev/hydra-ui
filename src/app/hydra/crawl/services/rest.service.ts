import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { TDomainQueue, isTDomainQueue } from 'hydra-crawler-ts-assets';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';

@Injectable()
export class RestService extends CommonsRestService {
	constructor(
			configService: CommonsConfigService,
			http: HttpClient
	) {
		super(
				CommonsType.assertString(configService.getString('rest', 'url')),
				http
		);
	}
	
	//----------------------------------------

	public async listQueueds(): Promise<TDomainQueue[]> {
		const rest: unknown = await super.get<TDomainQueue[]>('crawl/queue');
		if (!CommonsType.isTArray<TDomainQueue>(rest, isTDomainQueue)) throw new Error('Did not return an object');

		return rest;
	}

}
