import { Component } from '@angular/core';

import { TStatusTallies } from 'hydra-crawler-ts-assets';
import { EStatus } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { LiveService } from 'shared/services/live.service';

@Component({
	selector: 'dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent extends CommonsComponent {
	EStatus = EStatus;
	
	urlsTotal: number|undefined;
	urlsQueued: number|undefined;
	urlsDone: number|undefined;
	
	linkTallies: number|undefined;
	domainTallies: number|undefined;
	
	constructor(
			private liveService: LiveService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.liveService.statusTalliesObservable(),
				(tallies: TStatusTallies): void => {
					let total: number = 0;
					for (const tally of Object.values(tallies)) total += tally || 0;
					this.urlsTotal = total;
					
					this.urlsQueued = tallies[EStatus.QUEUED];
					this.urlsDone = tallies[EStatus.DONE];
				}
		);
		
		this.subscribe(
				this.liveService.linkTalliesObservable(),
				(tally: number): void => {
					this.linkTallies = tally;
				}
		);
		
		this.subscribe(
				this.liveService.domainTalliesObservable(),
				(tally: number): void => {
					this.domainTallies = tally;
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
