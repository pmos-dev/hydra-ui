import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsStockModule } from 'ngx-webappcommons-stock';

import { NgxGraphicsCommonsWidgetModule } from 'ngx-graphicscommons-widget';

import { SharedModule } from 'shared/shared.module';

import { DashboardComponent } from './components/dashboard/dashboard.component';

import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
	imports: [
		CommonModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxWebAppCommonsMobileModule,
		NgxWebAppCommonsStockModule,
		NgxGraphicsCommonsWidgetModule,
		SharedModule,
		DashboardRoutingModule
	],
	declarations: [
		DashboardComponent
	]
})
export class DashboardModule { }
