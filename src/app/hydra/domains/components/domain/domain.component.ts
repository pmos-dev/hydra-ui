import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CommonsTree, TSegmentTree, TSegmentStack } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'domain',
	templateUrl: './domain.component.html',
	styleUrls: ['./domain.component.less']
})
export class DomainComponent extends CommonsComponent {
	tree: TSegmentTree = { tally: 0 };
	stacks: TSegmentStack[][] = [];
	
	domain: string|undefined;

	constructor(
			private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		this.activatedRoute.params.subscribe((params: any): void => {
			this.domain = decodeURIComponent(params['domain']);
		});

		this.activatedRoute.data.subscribe((data: any): void => {
			this.tree = data['tree'];

			const stacks: TSegmentStack[][] = CommonsTree.treeToStacks(this.tree);
			this.stacks = stacks;
		});
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
