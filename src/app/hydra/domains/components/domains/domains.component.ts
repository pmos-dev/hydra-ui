import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'domains',
	templateUrl: './domains.component.html',
	styleUrls: ['./domains.component.less']
})
export class DomainsComponent extends CommonsComponent {
	constructor(
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
