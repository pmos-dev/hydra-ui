import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TDomain } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { RestService } from '../../services/rest.service';

@Component({
	selector: 'overview',
	templateUrl: './overview.component.html',
	styleUrls: ['./overview.component.less']
})
export class OverviewComponent extends CommonsComponent {

	domains: TDomain[] = [];

	domain: string|undefined;
	
	autocompleted: string[] = [];
	
	constructor(
			private router: Router,
			private activatedRoute: ActivatedRoute,
			private restService: RestService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		this.subscribe(
				this.activatedRoute.data,
				(data: {
						domains: TDomain[];
				}): void => {
					this.domains = data.domains
							.sort((a: TDomain, b: TDomain): number => {
								if (a.domain < b.domain) return -1;
								if (a.domain > b.domain) return 1;
								return 0;
							});
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	doSearch(): void {
		console.log(this.domain);
		this.router.navigate([ '..', 'domain', this.domain ], { relativeTo: this.activatedRoute });
	}
	
	async doAutocomplete(term: string): Promise<void> {
		const matches: string[] = await this.restService.listAutocompleteDomain(term);
		
		this.autocompleted = matches.sort();
	}

	doClick(domain: string): void {
		this.domain = domain;
	}
}
