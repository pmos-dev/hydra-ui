import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DomainsComponent } from './components/domains/domains.component';
import { OverviewComponent } from './components/overview/overview.component';
import { DomainComponent } from './components/domain/domain.component';

import { DomainsResolver } from './resolvers/domains.resolver';
import { DomainTreeResolver } from './resolvers/domain-tree.resolver';

const domainsRoutes: Routes = [
		{
			path: '',
			component: DomainsComponent,
			pathMatch: 'prefix',
			children: [
					{
						path: 'overview',
						component: OverviewComponent,
						resolve: {
							domains: DomainsResolver
						}
					},
					{
						path: 'domain/:domain',
						component: DomainComponent,
						resolve: {
							tree: DomainTreeResolver
						}
					},
					{
						path: '',
						redirectTo: 'overview'
					}
			]
		}
];

@NgModule({
	imports: [
		RouterModule.forChild(domainsRoutes)
	],
	exports: [
		RouterModule
	]
})
export class DomainsRoutingModule { }
