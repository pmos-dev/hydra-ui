import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';

import { DomainsComponent } from './components/domains/domains.component';
import { OverviewComponent } from './components/overview/overview.component';
import { DomainComponent } from './components/domain/domain.component';

import { RestService } from './services/rest.service';

import { DomainsResolver } from './resolvers/domains.resolver';
import { DomainTreeResolver } from './resolvers/domain-tree.resolver';

import { DomainsRoutingModule } from './domains-routing.module';

@NgModule({
	imports: [
		CommonModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxWebAppCommonsMobileModule,
		NgxWebAppCommonsFormModule,
		DomainsRoutingModule
	],
	providers: [
		RestService,
		DomainsResolver,
		DomainTreeResolver
	],
	declarations: [DomainsComponent, OverviewComponent, DomainComponent]
})
export class DomainsModule { }
