import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { TSegmentTree } from 'tscommons-core';

import { RestService } from '../services/rest.service';

@Injectable()
export class DomainTreeResolver implements Resolve<TSegmentTree> {
	constructor(
			private restService: RestService
	) {}

	resolve(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<TSegmentTree> {
		const domain: string = route.params['domain'];
		return this.restService.getDomainTree(domain);
	}
}
