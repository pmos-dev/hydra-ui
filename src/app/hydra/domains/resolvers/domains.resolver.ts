import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { TDomain } from 'hydra-crawler-ts-assets';

import { RestService } from '../services/rest.service';

@Injectable()
export class DomainsResolver implements Resolve<TDomain[]> {
	constructor(
			private restService: RestService
	) {}

	resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<TDomain[]> {
		return this.restService.listDomains();
	}
}
