import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';
import { TSegmentTree, isTSegmentTree } from 'tscommons-core';

import { TDomain, isTDomain } from 'hydra-crawler-ts-assets';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';

@Injectable()
export class RestService extends CommonsRestService {
	constructor(
			configService: CommonsConfigService,
			http: HttpClient
	) {
		super(
				CommonsType.assertString(configService.getString('rest', 'url')),
				http
		);
	}
	
	//----------------------------------------

	public async listDomains(): Promise<TDomain[]> {
		const rest: unknown = await super.get<TDomain[]>('domains');
		if (!CommonsType.isTArray<TDomain>(rest, isTDomain)) throw new Error('Did not return an array');
		
		return rest;
	}

	public async getDomainTree(domain: string): Promise<TSegmentTree> {
		const rest: unknown = await super.get<TSegmentTree[]>(`domains/${domain}/tree`);
		if (!isTSegmentTree(rest)) throw new Error('Did not return a segment tree');
		
		return rest;
	}
	
	//----------------------------------------
	
	public async listAutocompleteDomain(term: string): Promise<string[]> {
		const rest: unknown = await super.get<string[]>(`autocomplete/domain/${term.replace(/[^-a-z0-9.]/g, '')}`);
		if (!CommonsType.isStringArray(rest)) return [];
		
		return rest as string[];
	}

}
