import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgxWebAppCommonsStockModule } from 'ngx-webappcommons-stock';

import { HydraComponent } from './components/hydra/hydra.component';

const hydraRoutes: Routes = [
		{
			path: '',
			component: HydraComponent,
			children: [
				{
					path: 'dashboard',
					loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
				},
				{
					path: 'crawl',
					loadChildren: () => import('./crawl/crawl.module').then(m => m.CrawlModule)
				},
				{
					path: 'domains',
					loadChildren: () => import('./domains/domains.module').then(m => m.DomainsModule)
				},
				{
					path: 'urls',
					loadChildren: () => import('./urls/urls.module').then(m => m.UrlsModule)
				},
				{
					path: 'images',
					loadChildren: () => import('./images/images.module').then(m => m.ImagesModule)
				},
				{
					path: 'bugs',
					loadChildren: () => import('./bugs/bugs.module').then(m => m.BugsModule)
				},
				{
					path: '',
					redirectTo: 'dashboard'
				}
			]
		}
];

@NgModule({
	imports: [
		RouterModule.forChild(hydraRoutes),
		NgxWebAppCommonsStockModule
	],
	declarations: [
	],
	exports: [
		RouterModule
	]
})
export class HydraRoutingModule { }
