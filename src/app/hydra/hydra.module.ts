import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';

import { HydraComponent } from './components/hydra/hydra.component';

import { HydraRoutingModule } from './hydra-routing.module';

@NgModule({
	imports: [
		CommonModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxWebAppCommonsMobileModule,
		NgxWebAppCommonsFormModule,
		HydraRoutingModule
	],
	declarations: [
		HydraComponent
	]
})
export class HydraModule { }
