import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'images',
	templateUrl: './images.component.html',
	styleUrls: ['./images.component.less']
})
export class ImagesComponent extends CommonsComponent {

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
