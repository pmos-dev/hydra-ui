import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IImage } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'size',
	templateUrl: './size.component.html',
	styleUrls: ['./size.component.less']
})
export class SizeComponent extends CommonsComponent {
	images: IImage[] = [];

	constructor(
			private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.activatedRoute.data.subscribe((data: any): void => {
			this.images = data['images']
					.sort((a: IImage, b: IImage): number => {
						if (a.size < b.size) return 1;
						if (a.size > b.size) return -1;
						return 0;
					});
		});
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
