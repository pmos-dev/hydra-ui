import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImagesComponent } from './components/images/images.component';
import { OverviewComponent } from './components/overview/overview.component';
import { SizeComponent } from './components/size/size.component';

import { LargeImageResolver } from './resolvers/large-image.resolver';

const imagesRoutes: Routes = [
		{
			path: '',
			component: ImagesComponent,
			pathMatch: 'prefix',
			children: [
					{
						path: 'overview',
						component: OverviewComponent,
						resolve: {
						}
					},
					{
						path: 'size',
						component: SizeComponent,
						resolve: {
							images: LargeImageResolver
						}
					},
					{
						path: '',
						redirectTo: 'overview'
					}
			]
		}
];

@NgModule({
	imports: [
		RouterModule.forChild(imagesRoutes)
	],
	exports: [
		RouterModule
	]
})
export class ImagesRoutingModule { }
