import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';

import { ImagesComponent } from './components/images/images.component';
import { OverviewComponent } from './components/overview/overview.component';
import { SizeComponent } from './components/size/size.component';

import { RestService } from './services/rest.service';

import { LargeImageResolver } from './resolvers/large-image.resolver';

import { ImagesRoutingModule } from './images-routing.module';

@NgModule({
	imports: [
		CommonModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxWebAppCommonsMobileModule,
		NgxWebAppCommonsFormModule,
		NgxAngularCommonsPipeModule,
		ImagesRoutingModule
	],
	providers: [
		RestService,
		LargeImageResolver
	],
	declarations: [ImagesComponent, OverviewComponent, SizeComponent]
})
export class ImagesModule { }
