import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { IImage } from 'hydra-crawler-ts-assets';

import { RestService } from '../services/rest.service';

@Injectable()
export class LargeImageResolver implements Resolve<IImage[]> {
	constructor(
			private restService: RestService
	) {}

	resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<IImage[]> {
		return this.restService.listLargeImages(10000000);
	}
}
