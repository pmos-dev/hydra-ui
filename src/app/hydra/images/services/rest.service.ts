import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { IImage, isIImage } from 'hydra-crawler-ts-assets';
import { EComparator, fromEComparator } from 'hydra-crawler-ts-assets';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';

@Injectable()
export class RestService extends CommonsRestService {
	constructor(
			configService: CommonsConfigService,
			http: HttpClient
	) {
		super(
				CommonsType.assertString(configService.getString('rest', 'url')),
				http
		);
	}
	
	//----------------------------------------

	public async listLargeImages(size: number): Promise<IImage[]> {
		const rest: any = await super.get<any[]>(`images/${fromEComparator(EComparator.GT)}/${size}`);
		
		if (!Array.isArray(rest)) throw new Error('Did not return an array');

		const typecast: IImage[] = [];
		for (const item of rest) {
			if (!isIImage(item)) throw new Error('Invalid IImage object');
			typecast.push(item);
		}

		return typecast;
	}

}
