import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'overview',
	templateUrl: './overview.component.html',
	styleUrls: ['./overview.component.less']
})
export class OverviewComponent extends CommonsComponent {
	url: string|undefined;

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	doSearch(): void {
		if (!this.url) return;
		
		console.log(this.url);
		this.router.navigate([ '..', 'url', encodeURIComponent(this.url) ], { relativeTo: this.activatedRoute });
	}
}
