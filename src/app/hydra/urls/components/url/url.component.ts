import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TUrlLinks } from 'hydra-crawler-ts-assets';
import { EDirection } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'url',
	templateUrl: './url.component.html',
	styleUrls: ['./url.component.less']
})
export class UrlComponent extends CommonsComponent {
	EDirection = EDirection;
	encodeURIComponent = encodeURIComponent;

	url: string|undefined;
	urlLinks: TUrlLinks = { [EDirection.INBOUND]: [], [EDirection.OUTBOUND]: [] };
	
	constructor(
			private router: Router,
			private activatedRoute: ActivatedRoute
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		this.activatedRoute.params.subscribe((params: any): void => {
			this.url = decodeURIComponent(params['url']);
		});
	
		this.activatedRoute.data.subscribe((data: any): void => {
			this.urlLinks = data['links'];
		});
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}
	
	doNavigate(link: string): void {
		this.router.navigate([ '..', encodeURIComponent(link) ], { relativeTo: this.activatedRoute });
	}

}
