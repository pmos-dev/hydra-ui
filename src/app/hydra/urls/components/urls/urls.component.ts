import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'urls',
	templateUrl: './urls.component.html',
	styleUrls: ['./urls.component.less']
})
export class UrlsComponent extends CommonsComponent {
	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
