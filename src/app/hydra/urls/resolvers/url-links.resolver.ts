import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { TUrlLinks } from 'hydra-crawler-ts-assets';

import { RestService } from '../services/rest.service';

@Injectable()
export class UrlLinksResolver implements Resolve<TUrlLinks> {
	constructor(
			private restService: RestService
	) {}

	resolve(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<TUrlLinks> {
		const url: string = decodeURIComponent(route.params['url']);
		return this.restService.listUrlLinks(url);
	}
}
