import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { TUrlLinks, isTUrlLinks } from 'hydra-crawler-ts-assets';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';

@Injectable()
export class RestService extends CommonsRestService {
	constructor(
			configService: CommonsConfigService,
			http: HttpClient
	) {
		super(
				CommonsType.assertString(configService.getString('rest', 'url')),
				http
		);
	}
	
	//----------------------------------------

	public async listUrlLinks(url: string): Promise<TUrlLinks> {
		const rest: any = await super.get<any[]>(`urls/${encodeURIComponent(url)}/links`);
		
		if (!isTUrlLinks(rest)) throw new Error('Did not return a TUrlLinks');
		
		return rest;
	}

}
