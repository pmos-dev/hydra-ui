import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UrlsComponent } from './components/urls/urls.component';
import { OverviewComponent } from './components/overview/overview.component';
import { UrlComponent } from './components/url/url.component';

import { UrlLinksResolver } from './resolvers/url-links.resolver';

const urlsRoutes: Routes = [
		{
			path: '',
			component: UrlsComponent,
			pathMatch: 'prefix',
			children: [
					{
						path: 'overview',
						component: OverviewComponent,
						resolve: {
						}
					},
					{
						path: 'url/:url',
						component: UrlComponent,
						resolve: {
							links: UrlLinksResolver
						}
					},
					{
						path: '',
						redirectTo: 'overview'
					}
			]
		}
];

@NgModule({
	imports: [
		RouterModule.forChild(urlsRoutes)
	],
	exports: [
		RouterModule
	]
})
export class UrlsRoutingModule { }
