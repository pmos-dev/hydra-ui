import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';

import { UrlsComponent } from './components/urls/urls.component';
import { OverviewComponent } from './components/overview/overview.component';
import { UrlComponent } from './components/url/url.component';

import { RestService } from './services/rest.service';

import { UrlLinksResolver } from './resolvers/url-links.resolver';

import { UrlsRoutingModule } from './urls-routing.module';

@NgModule({
	imports: [
		CommonModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxWebAppCommonsMobileModule,
		NgxWebAppCommonsFormModule,
		UrlsRoutingModule
	],
	providers: [
		RestService,
		UrlLinksResolver
	],
	declarations: [UrlsComponent, OverviewComponent, UrlComponent]
})
export class UrlsModule { }
