import { Component } from '@angular/core';
import { NgZone, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';

import { ECommonsInvocation } from 'tscommons-async';

import { CommonsManualChangeDetectionComponent } from 'ngx-angularcommons-core';

import { ICommonsBar } from 'ngx-graphicscommons-chart';
import { ECommonsLabelType } from 'ngx-graphicscommons-chart';

import { LiveService } from '../../services/live.service';

@Component({
	selector: 'sandbox-bandwidth',
	templateUrl: './sandbox-bandwidth.component.html',
	styleUrls: ['./sandbox-bandwidth.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SandboxBandwidthComponent extends CommonsManualChangeDetectionComponent {
	ECommonsLabelType = ECommonsLabelType;

	private current = 0;
	private history: number[] = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

	bars: ICommonsBar[] = [];
	maxValue = 0;

	constructor(
			zone: NgZone,
			changeDetector: ChangeDetectorRef,
			private liveService: LiveService
	) {
		super(zone, changeDetector);
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.liveService.bandwidthObservable(),
				(bandwidth: number): void => {
					this.current += bandwidth;
				}
		);

		this.setInterval(
				1 * 1000,
				1 * 1000,
				ECommonsInvocation.IF,
				async (): Promise<void> => {
					this.history.push(this.current);
					this.history.shift();
					this.current = 0;

					this.refresh();
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	private refresh(): void {
		const now: Date = new Date();
		now.setSeconds(now.getSeconds() - this.history.length);

		this.maxValue = Math.max(...[ 1, ...this.history ]);

		this.bars = this.history
				.map((tally: number): ICommonsBar => {
					let h: string = now.getHours().toString();
					while (h.length < 2) { h = `0${h}`; }

					let m: string = now.getMinutes().toString();
					while (m.length < 2) { m = `0${m}`; }

					let s: string = now.getSeconds().toString();
					while (s.length < 2) { s = `0${s}`; }

					const result: ICommonsBar = {
						name: `${m}:${s}`,
						value: tally,
						color: 'primary',
						major: false
					};

					now.setSeconds(now.getSeconds() + 1);

					return result;
				});

		this.runChangeDetection();
	}
}
