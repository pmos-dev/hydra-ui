import { Component } from '@angular/core';

import { TOutcome } from 'hydra-crawler-ts-assets';
import { TFetch } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { LiveService } from 'shared/services/live.service';

enum EOutcome {
	INFORMATION = 'information',
	SUCCESS = 'success',
	REDIRECT = 'redirect',
	CLIENTERROR = 'clienterror',
	SERVERERROR = 'servererror',
	FAILED = 'failed',
	UNKNOWN = 'unknown'
}

interface TUrl {
	index: number;
	url: string;
	outcome?: EOutcome;
}

@Component({
	selector: 'sandbox-fetch',
	templateUrl: './sandbox-fetch.component.html',
	styleUrls: ['./sandbox-fetch.component.less']
})
export class SandboxFetchComponent extends CommonsComponent {
	EOutcome = EOutcome;

	urls: TUrl[] = [];

	constructor(
			private liveService: LiveService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.liveService.fetchingObservable(),
				(fetch: TFetch): void => {
					this.urls.unshift({
							index: fetch.index,
							url: fetch.url
					});
					if (this.urls.length > 21) { this.urls.pop(); }
				}
		);

		this.subscribe(
				this.liveService.outcomeObservable(),
				(outcome: TOutcome): void => {
					const existing: TUrl|undefined = this.urls
							.find((u: TUrl): boolean => u.index === outcome.index);

					if (!existing) { return; }

					if (outcome.failed) {
						existing.outcome = EOutcome.FAILED;
					} else {
						if (outcome.statusCode! >= 100 && outcome.statusCode! < 200) { existing.outcome = EOutcome.INFORMATION; }
						if (outcome.statusCode! >= 200 && outcome.statusCode! < 300) { existing.outcome = EOutcome.SUCCESS; }
						if (outcome.statusCode! >= 300 && outcome.statusCode! < 400) { existing.outcome = EOutcome.REDIRECT; }
						if (outcome.statusCode! >= 400 && outcome.statusCode! < 500) { existing.outcome = EOutcome.CLIENTERROR; }
						if (outcome.statusCode! >= 500 && outcome.statusCode! < 600) { existing.outcome = EOutcome.SERVERERROR; }
					}
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
