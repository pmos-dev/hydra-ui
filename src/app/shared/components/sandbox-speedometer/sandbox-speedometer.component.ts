import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { LiveService } from '../../services/live.service';

@Component({
	selector: 'sandbox-speedometer',
	templateUrl: './sandbox-speedometer.component.html',
	styleUrls: ['./sandbox-speedometer.component.less']
})
export class SandboxSpeedometerComponent extends CommonsComponent {
	bps = 0;

	constructor(
			private liveService: LiveService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.liveService.bandwidthObservable(),
				(bps: number): void => {
					this.bps = bps;
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

}
