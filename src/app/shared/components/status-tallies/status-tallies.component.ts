import { Component, Input } from '@angular/core';

import { TStatusTallies } from 'hydra-crawler-ts-assets';
import { EStatus, fromEStatus } from 'hydra-crawler-ts-assets';

import { CommonsComponent } from 'ngx-angularcommons-core';
import { CommonsUcWordsPipe } from 'ngx-angularcommons-pipe';

import { LiveService } from 'shared/services/live.service';

@Component({
	selector: 'status-tallies',
	templateUrl: './status-tallies.component.html',
	styleUrls: ['./status-tallies.component.less']
})
export class StatusTalliesComponent extends CommonsComponent {
	@Input() statuses: EStatus[] = [];

	tallies: Map<string, number|undefined> = new Map<string, number|undefined>();

	constructor(
			private liveService: LiveService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.liveService.statusTalliesObservable(),
				(tallies: TStatusTallies): void => {
					for (const key of this.statuses) {
						const s: string = new CommonsUcWordsPipe().transform(fromEStatus(key));
						this.tallies.set(s, tallies[key]);
					}
				}
		);
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
	}

	getLabels(): string[] {
		return this.statuses
				.map((status: EStatus): string => new CommonsUcWordsPipe().transform(fromEStatus(status)));
	}
}
