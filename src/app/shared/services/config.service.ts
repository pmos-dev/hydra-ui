import { Injectable } from '@angular/core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsSocketIoUrlOptions } from 'ngx-httpcommons-socket-io';

import { THydraConfig, isTHydraConfig } from '../types/thydra-config';

@Injectable({
		providedIn: 'root'
})
export class ConfigService {
	private config: THydraConfig;
	
	constructor(
			configService: CommonsConfigService
	) {
		const config: unknown = configService.getDirect('hydra');
		if (!isTHydraConfig(config)) throw new Error(`Unable to read URL from hydra config`);
		
		this.config = config;
	}
	
	public getConfig(): THydraConfig {
		return this.config;
	}

	public getUrl(): string {
		return this.config.url;
	}
	
	public getSocketIoUrl(): string {
		return CommonsSocketIoUrlOptions.buildUrl(this.getUrl());
	}
	
	public getSocketIoOptions(): SocketIOClient.ConnectOpts {
		return CommonsSocketIoUrlOptions.buildOptions(this.getUrl());
	}
}
