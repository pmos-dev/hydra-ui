import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsType } from 'tscommons-core';

import { TStatusTallies, isTStatusTallies } from 'hydra-crawler-ts-assets';
import { TOutcome, isTOutcome } from 'hydra-crawler-ts-assets';
import { TFetch, isTFetch } from 'hydra-crawler-ts-assets';

import { CommonsSocketIoClientService } from 'ngx-httpcommons-socket-io';

import { ConfigService } from './config.service';

@Injectable({
	providedIn: 'root'
})
export class LiveService extends CommonsSocketIoClientService {

	private onFetching: EventEmitter<TFetch> = new EventEmitter<TFetch>(true);
	private onOutcome: EventEmitter<TOutcome> = new EventEmitter<TOutcome>(true);
	private onStatusTallies: EventEmitter<TStatusTallies> = new EventEmitter<TStatusTallies>(true);
	private onLinkTallies: EventEmitter<number> = new EventEmitter<number>(true);
	private onDomainTallies: EventEmitter<number> = new EventEmitter<number>(true);
	private onBandwidth: EventEmitter<number> = new EventEmitter<number>(true);

	constructor(
			configService: ConfigService
	) {
		super(
				configService.getSocketIoUrl(),
				true,
				configService.getSocketIoOptions()
		);
		
		this.connect();
	}

	protected setupOns(): void {
		super.on('fetching', (data: any): void => {
			if (!isTFetch(data)) { throw new Error('Unable to parse fetching'); }
			this.onFetching.emit(data);
		});

		super.on('outcome', (data: any): void => {
			if (!isTOutcome(data)) { throw new Error('Unable to parse outcome'); }
			this.onOutcome.emit(data);
		});

		super.on('statusTallies', (data: any): void => {
			if (!isTStatusTallies(data)) { throw new Error('Unable to parse status tallies'); }
			this.onStatusTallies.emit(data);
		});

		super.on('linkTallies', (data: any): void => {
			CommonsType.assertNumber(data);
			this.onLinkTallies.emit(data as number);
		});

		super.on('domainTallies', (data: any): void => {
			CommonsType.assertNumber(data);
			this.onDomainTallies.emit(data as number);
		});

		super.on('bandwidth', (data: any): void => {
			if ('number' !== typeof data) { throw new Error('Unable to parse bandwidth bps'); }
			this.onBandwidth.emit(data);
		});
	}

	public fetchingObservable(): Observable<TFetch> {
		return this.onFetching;
	}

	public outcomeObservable(): Observable<TOutcome> {
		return this.onOutcome;
	}

	public statusTalliesObservable(): Observable<TStatusTallies> {
		return this.onStatusTallies;
	}

	public linkTalliesObservable(): Observable<number> {
		return this.onLinkTallies;
	}

	public domainTalliesObservable(): Observable<number> {
		return this.onDomainTallies;
	}

	public bandwidthObservable(): Observable<number> {
		return this.onBandwidth;
	}
}
