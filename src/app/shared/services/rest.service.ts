import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType, TPropertyObject, TEncodedObject } from 'tscommons-core';

import { TStatusTallies, isTStatusTallies } from 'hydra-crawler-ts-assets';

import { CommonsRestService } from 'ngx-httpcommons-rest';

import { ConfigService } from '../services/config.service';

import { THydraConfig } from '../types/thydra-config';

@Injectable({
	providedIn: 'root'
})
export class RestService extends CommonsRestService {
	protected reattempts: number|undefined;

	constructor(
			http: HttpClient,
			private configService: ConfigService
	) {
		super(
				configService.getUrl(),
				http
		);

		const config: THydraConfig = this.configService.getConfig();

		if (config.timeout) {
			this.setTimeout(config.timeout);
		}

		this.reattempts = config.reattempts;

		if (config.reattemptTimeout) {
			this.setReattemptTimeout(config.reattemptTimeout);
		}
	}

	// ----------------------------------------

	private static parseStatusTallies(rest: any): TStatusTallies {
		if (!CommonsType.isEncodedObject(rest)) { throw new Error('Unable to parse status tallies'); }
		const encoded: TEncodedObject = rest as TEncodedObject;
		const object: TPropertyObject = CommonsType.decodePropertyObject(encoded);

		if (!isTStatusTallies(object)) { throw new Error('Object is not an instance of TStatusTallies'); }

		return object;
	}

	public async getStatusTallies(): Promise<TStatusTallies> {
		const rest: any = await super.get<any[]>(
				'statistics/status',
				undefined,
				this.reattempts
		);

		return RestService.parseStatusTallies(rest);
	}
}
