import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsStockModule } from 'ngx-webappcommons-stock';

import { NgxGraphicsCommonsCoreModule } from 'ngx-graphicscommons-core';
import { NgxGraphicsCommonsChartModule } from 'ngx-graphicscommons-chart';
import { NgxGraphicsCommonsWidgetModule } from 'ngx-graphicscommons-widget';

import { SandboxSpeedometerComponent } from './components/sandbox-speedometer/sandbox-speedometer.component';
import { SandboxBandwidthComponent } from './components/sandbox-bandwidth/sandbox-bandwidth.component';
import { SandboxFetchComponent } from './components/sandbox-fetch/sandbox-fetch.component';
import { StatusTalliesComponent } from './components/status-tallies/status-tallies.component';

import { RestService } from './services/rest.service';
import { LiveService } from './services/live.service';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		NgxWebAppCommonsCoreModule,
		NgxWebAppCommonsAppModule,
		NgxAngularCommonsPipeModule,
		NgxWebAppCommonsStockModule,
		NgxGraphicsCommonsCoreModule,
		NgxGraphicsCommonsChartModule,
		NgxGraphicsCommonsWidgetModule
	],
	declarations: [
		SandboxSpeedometerComponent,
		SandboxBandwidthComponent,
		SandboxFetchComponent,
		StatusTalliesComponent
	],
	exports: [
		SandboxSpeedometerComponent,
		SandboxBandwidthComponent,
		SandboxFetchComponent,
		StatusTalliesComponent
	]
})
export class SharedModule {
	static forRoot(): ModuleWithProviders<SharedModule> {
		return {
			ngModule: SharedModule,
			providers: [
				RestService,
				LiveService
			]
		};
	}
}
