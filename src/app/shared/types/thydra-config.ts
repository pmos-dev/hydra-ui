import { CommonsType } from 'tscommons-core';

export type THydraConfig = {
		url: string;
		refreshInterval: number;
		timeout?: number;
		reattempts?: number;
		reattemptTimeout?: number;
};

export function isTHydraConfig(test: unknown): test is THydraConfig {
	if (!CommonsType.hasPropertyString(test, 'url')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'refreshInterval')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'timeout')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'reattempts')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'reattemptTimeout')) return false;

	return true;
}
